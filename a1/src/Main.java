import java.util.*;

public class Main {
    public static void main(String[] args) {
        HashMap <String, Integer> games = new HashMap<>();
        games.put("Mario Odyssey", 50);
        games.put("Super Smash Bros. Ultimate", 20);
        games.put("Luigi's Mansion 3", 15);
        games.put("Pokemon Sword", 30);
        games.put("Pokemon Shield", 100);

        ArrayList<String> topGames = new ArrayList<>();
        games.forEach((key, value) -> {
            System.out.println(String.format("%s has %s stocks left.", key, value));

            if(value <= 30){
                topGames.add(key);
                System.out.println(String.format("%s has been added to top games list!", key));
            }
        });

        System.out.println(String.format("Our shop's top games: \n%s", topGames));
    }
}